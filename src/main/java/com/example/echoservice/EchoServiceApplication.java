package com.example.echoservice;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import jdk.jfr.DataAmount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class EchoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EchoServiceApplication.class, args);
	}

}

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class ResponseDto {

  private String httpMethod;
  private String querySting;
  private String requestBody;
  private String path;
  private Map<String, String[]> params;

}

@RestController
class EchoController {
  @RequestMapping
  public ResponseDto echo(HttpServletRequest request) throws IOException {
    return ResponseDto.builder()
      .path(request.getRequestURI())
      .httpMethod(request.getMethod())
      .querySting(request.getQueryString())
      .requestBody(request.getReader().lines().collect(Collectors.joining(System.lineSeparator())))
      .params(request.getParameterMap())
      .build();
  }
}
